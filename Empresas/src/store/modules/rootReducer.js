import { combineReducers } from 'redux';

import auth from './auth/reducer';
import enterprise from './enterprise/reducer';
import detail from './detail/reducer';

export default combineReducers({ auth, enterprise, detail });
