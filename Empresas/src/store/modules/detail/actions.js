export const showDetailRequest = id => ({
  type: '@detail/SHOW_DETAIL_REQUEST',
  payload: { id },
});

export const showDetailSuccess = enterprise => ({
  type: '@detail/SHOW_DETAIL_SUCCESS',
  payload: { enterprise },
});

export const showDetailFailure = () => ({
  type: '@detail/SHOW_DETAIL_FAILURE',
});
