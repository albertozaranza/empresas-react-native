import produce from 'immer';

const INITIAL_STATE = {
  photo: null,
  enterprise_name: '',
  city: '',
  country: '',
  share_price: '',
  description: '',
  loading: false,
};

const detail = (state = INITIAL_STATE, action) => {
  return produce(state, draft => {
    switch (action.type) {
      case '@detail/SHOW_DETAIL_REQUEST': {
        draft.loading = true;
        break;
      }
      case '@detail/SHOW_DETAIL_SUCCESS': {
        draft.photo = action.payload.enterprise.photo;
        draft.enterprise_name = action.payload.enterprise.enterprise_name;
        draft.city = action.payload.enterprise.city;
        draft.country = action.payload.enterprise.country;
        draft.share_price = action.payload.enterprise.share_price;
        draft.description = action.payload.enterprise.description;
        draft.loading = false;
        break;
      }
      case '@detail/SHOW_DETAIL_FAILURE': {
        draft.loading = false;
        break;
      }
      default:
    }
  });
};

export default detail;
