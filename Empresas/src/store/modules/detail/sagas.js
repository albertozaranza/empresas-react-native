import { Alert } from 'react-native';
import { takeLatest, call, put, all } from 'redux-saga/effects';

import { showDetailSuccess, showDetailFailure } from './actions';

import api from '~/services/api';

export function* showDetail({ payload }) {
  try {
    const { id } = payload;

    const response = yield call(api.get, `enterprises/${id}`);

    const { enterprise } = response.data;

    yield put(showDetailSuccess(enterprise));
  } catch (err) {
    Alert.alert('Falha na busca', 'Os dados não puderam ser carregados');

    yield put(showDetailFailure());
  }
}

export default all([takeLatest('@detail/SHOW_DETAIL_REQUEST', showDetail)]);
