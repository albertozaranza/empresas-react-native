import produce from 'immer';

const INITIAL_STATE = {
  access_token: null,
  client: null,
  uid: null,
  expiry: null,
  signed: false,
  loading: false,
};

const auth = (state = INITIAL_STATE, action) => {
  return produce(state, draft => {
    switch (action.type) {
      case '@auth/SIGN_IN_REQUEST': {
        draft.loading = true;
        break;
      }
      case '@auth/SIGN_IN_SUCCESS': {
        draft.access_token = action.payload.access_token;
        draft.client = action.payload.client;
        draft.uid = action.payload.uid;
        draft.expiry = action.payload.expiry;
        draft.signed = true;
        draft.loading = false;
        break;
      }
      case '@auth/SIGN_FAILURE': {
        draft.loading = false;
        break;
      }
      case '@auth/SIGN_OUT': {
        draft.access_token = null;
        draft.client = null;
        draft.uid = null;
        draft.expiry = null;
        draft.signed = false;
        draft.loading = false;
        break;
      }
      default:
    }
  });
};

export default auth;
