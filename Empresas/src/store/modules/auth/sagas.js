import { Alert } from 'react-native';
import { takeLatest, call, put, all } from 'redux-saga/effects';

import { signInSuccess, signFailure } from './actions';

import api from '~/services/api';

export function* signIn({ payload }) {
  try {
    const { email, password } = payload;

    const response = yield call(api.post, 'users/auth/sign_in', {
      email,
      password,
    });

    const {
      'access-token': access_token,
      client,
      uid,
      expiry,
    } = response.headers;

    if (!uid) {
      Alert.alert('Erro no login', 'Usuário não cadastrado');
      return;
    }

    api.defaults.headers = {
      ...api.defaults.headers,
      'access-token': access_token,
      client,
      uid,
      expiry,
    };

    yield put(signInSuccess(access_token, client, uid, expiry));
  } catch (err) {
    Alert.alert(
      'Falha na autenticação',
      'Houve um erro no login, verifique seus dados'
    );

    yield put(signFailure());
  }
}

export function setToken({ payload }) {
  if (!payload) return;

  const { access_token, client, uid, expiry } = payload.auth;

  if (access_token && client && uid && expiry) {
    api.defaults.headers = {
      ...api.defaults.headers,
      'access-token': access_token,
      client,
      uid,
      expiry,
    };
  }
}

export default all([
  takeLatest('persist/REHYDRATE', setToken),
  takeLatest('@auth/SIGN_IN_REQUEST', signIn),
]);
