export const signInRequest = (email, password) => {
  return {
    type: '@auth/SIGN_IN_REQUEST',
    payload: { email, password },
  };
};

export const signInSuccess = (access_token, client, uid, expiry) => {
  return {
    type: '@auth/SIGN_IN_SUCCESS',
    payload: { access_token, client, uid, expiry },
  };
};

export const signFailure = () => {
  return {
    type: '@auth/SIGN_FAILURE',
  };
};

export const signOut = () => {
  return {
    type: '@auth/SIGN_OUT',
  };
};
