export const listAllEnterprisesRequest = () => ({
  type: '@enterprise/LIST_ALL_ENTERPRISES_REQUEST',
});

export const listAllEnterprisesSuccess = enterprises => ({
  type: '@enterprise/LIST_ALL_ENTERPRISES_SUCCESS',
  payload: { enterprises },
});

export const listAllEnterprisesFailure = () => ({
  type: '@enterprise/LIST_ALL_ENTERPRISES_FAILURE',
});

export const listSpecificEnterpriseRequest = value => ({
  type: '@enterprise/LIST_SPECIFIC_ENTERPRISE_REQUEST',
  payload: { value },
});

export const listSpecificEnterpriseSuccess = enterprises => ({
  type: '@enterprise/LIST_SPECIFIC_ENTERPRISE_SUCCESS',
  payload: { enterprises },
});

export const listSpecificEnterpriseFailure = () => ({
  type: '@enterprise/LIST_SPECIFIC_ENTERPRISE_FAILURE',
});
