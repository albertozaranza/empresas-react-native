import { Alert } from 'react-native';
import { takeLatest, call, put, all } from 'redux-saga/effects';

import {
  listAllEnterprisesSuccess,
  listAllEnterprisesFailure,
  listSpecificEnterpriseSuccess,
  listSpecificEnterpriseFailure,
} from './actions';

import api from '~/services/api';

export function* listAllEnterprises() {
  try {
    const response = yield call(api.get, 'enterprises');

    const { enterprises } = response.data;

    yield put(listAllEnterprisesSuccess(enterprises));
  } catch (err) {
    Alert.alert('Falha na busca', 'Os dados não puderam ser carregados');

    yield put(listAllEnterprisesFailure());
  }
}

export function* listSpecificEnterprise({ payload }) {
  try {
    const { value } = payload;

    const response = yield call(api.get, `enterprises?name=${value}`);

    const { enterprises } = response.data;

    yield put(listSpecificEnterpriseSuccess(enterprises));
  } catch (err) {
    Alert.alert('Falha na busca', 'Os dados não puderam ser carregados');

    yield put(listSpecificEnterpriseFailure());
  }
}

export default all([
  takeLatest('@enterprise/LIST_ALL_ENTERPRISES_REQUEST', listAllEnterprises),
  takeLatest(
    '@enterprise/LIST_SPECIFIC_ENTERPRISE_REQUEST',
    listSpecificEnterprise
  ),
]);
