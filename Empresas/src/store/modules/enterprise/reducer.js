import produce from 'immer';

const INITIAL_STATE = {
  enterprises: [],
  loading: false,
};

const enterprise = (state = INITIAL_STATE, action) => {
  return produce(state, draft => {
    switch (action.type) {
      case '@enterprise/LIST_ALL_ENTERPRISES_REQUEST': {
        draft.loading = true;
        break;
      }
      case '@enterprise/LIST_ALL_ENTERPRISES_SUCCESS': {
        draft.enterprises = action.payload.enterprises;
        draft.loading = false;
        break;
      }
      case '@enterprise/LIST_ALL_ENTERPRISES_FAILURE': {
        draft.loading = false;
        break;
      }
      case '@enterprise/LIST_SPECIFIC_ENTERPRISE_REQUEST': {
        draft.loading = true;
        break;
      }
      case '@enterprise/LIST_SPECIFIC_ENTERPRISE_SUCCESS': {
        draft.enterprises = action.payload.enterprises;
        draft.loading = false;
        break;
      }
      case '@enterprise/LIST_SPECIFIC_ENTERPRISE_FAILURE': {
        draft.loading = false;
        break;
      }
      default:
    }
  });
};

export default enterprise;
