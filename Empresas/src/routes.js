import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import SignIn from '~/pages/SignIn';
import Dashboard from '~/pages/Dashboard';
import Detail from '~/pages/Detail';

const Routes = (signedIn = false) => {
  return createAppContainer(
    createSwitchNavigator(
      {
        Sign: createSwitchNavigator({
          SignIn,
        }),
        App: createStackNavigator({
          Dashboard: {
            screen: Dashboard,
            navigationOptions: () => ({
              headerTransparent: true,
            }),
          },
          Detail: {
            screen: Detail,
            navigationOptions: () => ({
              headerTransparent: true,
              headerTintColor: '#fff',
            }),
          },
        }),
      },
      {
        initialRouteName: signedIn ? 'App' : 'Sign',
      }
    )
  );
};

export default Routes;
