import React from 'react';
import { ActivityIndicator } from 'react-native';
import { RectButton } from 'react-native-gesture-handler';
import styled from 'styled-components/native';
import PropTypes from 'prop-types';

const Button = ({ children, loading, ...props }) => {
  return (
    <StyledContainer {...props}>
      {loading ? (
        <ActivityIndicator size="small" color="#fff" />
      ) : (
        <StyledText>{children}</StyledText>
      )}
    </StyledContainer>
  );
};

Button.propTypes = {
  children: PropTypes.node.isRequired,
  loading: PropTypes.bool.isRequired,
};

const StyledContainer = styled(RectButton)`
  height: 46px;
  justify-content: center;
  align-items: center;
  border-radius: 4px;
  background: #3b9eff;
`;

const StyledText = styled.Text`
  font-size: 16px;
  font-weight: bold;
  color: #fff;
`;

export default Button;
