import React from 'react';
import { TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import styled from 'styled-components/native';
import { useNavigation } from 'react-navigation-hooks';
import PropTypes from 'prop-types';

const Enterprise = ({ data: { id, enterprise_name, city, country } }) => {
  const { navigate } = useNavigation();

  function handleDetail() {
    navigate('Detail', { id });
  }

  return (
    <StyledContainer>
      <StyledHeader>
        <StyledPhoto
          source={{
            uri: `https://picsum.photos/seed/${Math.random() * 100}/150/75`,
          }}
        />
      </StyledHeader>
      <StyledFooter>
        <StyledInfo>
          <StyledName>{enterprise_name}</StyledName>
          <StyledLocalization>
            {city}, {country}
          </StyledLocalization>
        </StyledInfo>
        <TouchableOpacity onPress={handleDetail}>
          <Icon name="info" size={30} color="#00B4DB" />
        </TouchableOpacity>
      </StyledFooter>
    </StyledContainer>
  );
};

Enterprise.propTypes = {
  data: PropTypes.shape({
    id: PropTypes.number,
    photo: PropTypes.string,
    enterprise_name: PropTypes.string,
    city: PropTypes.string,
    country: PropTypes.string,
  }).isRequired,
};

const StyledContainer = styled.View`
  margin-bottom: 15px;
  padding: 20px;
  border-radius: 4px;
  background: #fff;
`;

const StyledFooter = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

const StyledHeader = styled.View`
  justify-content: center;
  align-items: center;
  margin-bottom: 20px;
`;

const StyledPhoto = styled.Image`
  height: 100px;
  width: 325px;
`;

const StyledInfo = styled.View`
  flex: 1;
`;

const StyledName = styled.Text`
  font-size: 16px;
  font-weight: bold;
  color: #333;
`;

const StyledLocalization = styled.Text`
  font-size: 14px;
  color: #999;
`;

export default Enterprise;
