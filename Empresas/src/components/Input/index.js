import React, { forwardRef } from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import styled from 'styled-components/native';

// eslint-disable-next-line react/prop-types
const Input = ({ style, icon, ...props }, ref) => (
  <StyledContainer style={style}>
    {icon && <Icon name={icon} size={20} color="rgba(255,255,255, 0.6)" />}
    <StyledTInput {...props} ref={ref} />
  </StyledContainer>
);

const StyledContainer = styled.View`
  height: 46px;
  flex-direction: row;
  align-items: center;
  padding: 0 15px;
  border-radius: 4px;
  background: rgba(0, 0, 0, 0.1);
`;

const StyledTInput = styled.TextInput.attrs({
  placeholderTextColor: 'rgba(255,255,255,0.8)',
})`
  flex: 1;
  margin-left: 10px;
  font-size: 15px;
  color: #fff;
`;

export default forwardRef(Input);
