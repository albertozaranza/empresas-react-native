import React, { useEffect, useState } from 'react';
import { ActivityIndicator } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import lodash from 'lodash';
import styled from 'styled-components/native';

import Input from '~/components/Input';
import Background from '~/components/Background';
import Enterprise from '~/components/Enterprise';

import {
  listAllEnterprisesRequest,
  listSpecificEnterpriseRequest,
} from '~/store/modules/enterprise/actions';
import { signOut } from '~/store/modules/auth/actions';

const Dashboard = () => {
  const dispatch = useDispatch();
  const [currentEnterprise, setCurrentEnterprise] = useState('');
  const expiry = useSelector(({ auth }) => auth.expiry);
  const loading = useSelector(({ enterprise }) => enterprise.loading);
  const enterprises = useSelector(({ enterprise }) => enterprise.enterprises);

  useEffect(() => {
    if (expiry < Date.now() / 1000) {
      dispatch(signOut());
    }
    dispatch(listAllEnterprisesRequest());
  }, [dispatch, expiry]);

  useEffect(() => {
    if (expiry < Date.now() / 1000) {
      dispatch(signOut());
    }
    if (currentEnterprise !== '') {
      lodash.debounce(
        () => dispatch(listSpecificEnterpriseRequest(currentEnterprise)),
        200
      )();
    }
  }, [currentEnterprise, dispatch, expiry]);

  function renderList() {
    if (enterprises.length === 0) {
      return (
        <StyledNoResult length={enterprises.length} loading={loading}>
          Não há resultados
        </StyledNoResult>
      );
    }
    return (
      <StyledList
        data={enterprises}
        keyExtractor={item => String(item.id)}
        renderItem={({ item }) => <Enterprise data={item} />}
      />
    );
  }

  return (
    <Background>
      <StyledContainer>
        <StyledTitle>Empresas</StyledTitle>
        <StyledSearch>
          <StyledSearchInput
            placeholder="Pesquisar empresa"
            autoCorrect={false}
            autoCapitalize="none"
            value={currentEnterprise}
            onChangeText={setCurrentEnterprise}
          />
        </StyledSearch>
        {loading ? <StyledActivityIndicator /> : renderList()}
      </StyledContainer>
    </Background>
  );
};

const StyledContainer = styled.SafeAreaView`
  flex: 1;
`;

const StyledTitle = styled.Text`
  align-self: center;
  margin: 20px 0;
  font-size: 20px;
  font-weight: bold;
  color: #fff;
`;

const StyledNoResult = styled.Text`
  height: 100px;
  align-self: center;
  margin-top: 30px;
  font-size: 16px;
  color: #eee;
`;

const StyledList = styled.FlatList.attrs({
  showsVerticalScrollIndicator: false,
  contentContainerStyle: {
    padding: 20,
  },
})``;

const StyledSearch = styled.View`
  flex-direction: row;
  margin: 0 20px;
`;

const StyledActivityIndicator = styled(ActivityIndicator).attrs({
  size: 'large',
  color: '#fff',
})`
  margin-top: 20px;
`;

const StyledSearchInput = styled(Input)`
  flex: 4;
`;

export default Dashboard;
