import React, { useEffect } from 'react';
import { ActivityIndicator } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigationParam } from 'react-navigation-hooks';
import styled from 'styled-components/native';

import { showDetailRequest } from '~/store/modules/detail/actions';

const Detail = () => {
  const dispatch = useDispatch();
  const {
    enterprise_name,
    city,
    country,
    share_price,
    description,
    loading,
  } = useSelector(({ detail }) => detail);
  const id = useNavigationParam('id');

  useEffect(() => {
    dispatch(showDetailRequest(id));
  }, [dispatch, id]);

  return (
    <StyledContainer>
      {loading ? (
        <StyledActivityIndicator />
      ) : (
        <>
          <StyledHeader>
            <StyledPhoto
              source={{
                uri: `https://picsum.photos/seed/${Math.random() * 100}/150/75`,
              }}
            />
          </StyledHeader>
          <StyledMain>
            <StyledInfo>
              <StyledName>{enterprise_name}</StyledName>
              <StyledLocalization>
                {city}, {country}
              </StyledLocalization>
            </StyledInfo>
            <StyledPrice>$ {share_price}</StyledPrice>
          </StyledMain>
          <StyledFooter>
            <StyledDescription>{description}</StyledDescription>
          </StyledFooter>
        </>
      )}
    </StyledContainer>
  );
};

const StyledContainer = styled.View`
  flex: 1;
`;

const StyledActivityIndicator = styled(ActivityIndicator).attrs({
  size: 'large',
  color: '#00B4DB',
})`
  flex: 1;
  justify-content: center;
  align-items: center;
  margin-top: 20px;
`;

const StyledHeader = styled.View`
  justify-content: center;
  align-items: center;
  margin-bottom: 20px;
`;

const StyledPhoto = styled.Image`
  height: 300px;
  width: 500px;
`;

const StyledMain = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 20px;
  padding: 5px 10px;
`;

const StyledInfo = styled.View`
  flex: 1;
`;

const StyledName = styled.Text`
  font-size: 20px;
  font-weight: bold;
  color: #333;
`;

const StyledLocalization = styled.Text`
  font-size: 16px;
  color: #999;
`;

const StyledPrice = styled.Text`
  font-size: 20px;
  font-weight: bold;
  color: #222;
`;

const StyledFooter = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 10px;
  border-top-width: 2px;
  border-top-color: #eee;
`;

const StyledDescription = styled.Text`
  font-size: 14px;
  color: #999;
`;

export default Detail;
