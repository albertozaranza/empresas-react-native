import React, { useRef, useState } from 'react';
import { Platform } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components/native';

import Background from '~/components/Background';
import Input from '~/components/Input';
import Button from '~/components/Button';

import { signInRequest } from '~/store/modules/auth/actions';

import logo from '~/assets/logo.png';

const SignIn = () => {
  const passwordRef = useRef();
  const dispach = useDispatch();

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const loading = useSelector(({ auth }) => auth.loading);

  function handleSubmit() {
    dispach(signInRequest(email, password));
  }

  return (
    <Background>
      <StyledContainer>
        <StyledImage source={logo} />
        <StyledForm>
          <StyledFormInput
            icon="mail-outline"
            keyboardType="email-address"
            autoCorrect={false}
            autoCapitalize="none"
            placeholder="Digite seu e-mail"
            returnKeyType="next"
            onSubmitEditing={() => passwordRef.current.focus()}
            value={email}
            onChangeText={setEmail}
          />

          <StyledFormInput
            icon="lock-outline"
            secureTextEntry
            placeholder="Digite sua senha"
            ref={passwordRef}
            returnKeyType="send"
            onSubmitEditing={handleSubmit}
            value={password}
            onChangeText={setPassword}
          />

          <StyledSubmitButton loading={loading} onPress={handleSubmit}>
            Acessar
          </StyledSubmitButton>
        </StyledForm>
      </StyledContainer>
    </Background>
  );
};

const StyledContainer = styled.KeyboardAvoidingView.attrs({
  enabled: Platform.OS === 'ios',
  behavior: 'padding',
})`
  flex: 1;
  justify-content: center;
  align-items: center;
  padding: 0 10px;
`;

const StyledImage = styled.Image`
  height: 150;
  width: 150;
`;

const StyledForm = styled.View`
  align-self: stretch;
  margin-top: 50px;
`;

const StyledFormInput = styled(Input)`
  margin-bottom: 10px;
`;

const StyledSubmitButton = styled(Button)`
  margin-top: 5px;
`;

export default SignIn;
