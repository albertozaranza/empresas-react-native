# Desafio React Native - ioasys

Para conseguir rodar o projeto, realize os passos a seguir:

### 1. Instale as dependências:

> `$ yarn`

### 2. Instale o aplicativo:

> `$ react-native run-android`

ou

> `$ react-native run-ios`

**P.S.:** O projeto pode não funcionar em ambientes iOS por conta de algumas configurações que não são possíveis alterar, tendo em vista que a aplicação foi desenvolvida em ambiente Windows. Também será necessário acessar a pasta ~/ios e rodar o comando:

> `$ pod install`

## Desafios cumpridos

[x] Relização de login e acesso de usuário já registrado

[x] Uso dos três endpoints disponibilizados

[x] Utilização de mais de uma tela

[x] Filtro por tipo e nome da empresa (funcional mas o filtro por tipo está estático)

[x] Utilização de Redux / Redux Saga

[x] Utilização de linters ou outras ferramentas de análise estática

[ ] Testes unitários, interface, etc

## Bibliotecas utilizadas

- @react-native-community/async-storage - Armazenamento de dados locais para persitencia de dados do redux

- axios - Requisições HTTP

- immer - Persistencia de dados

- dotenv - Criação de variáveis de ambiente (não foi necessária sua utilização)

- react-navigation, react-navigation-stack, react-navigation-hooks, react-native-gesture-handler - Manipulação de telas

- react-native-linear-gradient - Criação do efeito gradiente

- react-native-vector-icons, react-native-reanimated, react-native-screens - Uso de ícones

- redux, react-redux, redux-saga, redux-persist - Uso de variáveis de estado global e persitência de dados

- reactotron-react-native, reactotron-redux, reactotron-redux-saga - Debug da aplicação

- styled-components - Estilização de componentes de uma forma menos verbosa

## Sugestões

- Anexar algum arquivo com os tipos de empresas existentes

- Verificação de dados do banco (algumas imagens estão inacessíveis)
